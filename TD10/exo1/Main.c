#include <pthread.h>
#include <math.h>
#include <stdlib.h>
#include <stdio.h>

void* calcul(void* arg){
    int* deb = (int*)arg;
    double *res=0; 
    res = (double *) malloc(sizeof(double));
    for(double indice=*deb;indice<=100;indice += 2){
        *res += sqrt(indice);
    }
    pthread_exit((void *) res);
}




int main()
{
    pthread_t T1, T2;
    int deb1 = 1, deb2 = 2;
    double *res1, *res2, somme;
    pthread_create(&T1, NULL, calcul, (void*) &deb1);
    pthread_create(&T2, NULL, calcul, (void*) &deb2);
    pthread_join(T1,(void**) &res1);
    pthread_join(T2,(void**) &res2);
    somme = *res1+ *res2;
    printf("%f \n",somme);
}