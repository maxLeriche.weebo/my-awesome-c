#include <pthread.h>
#include <math.h>
#include <stdlib.h>
#include <stdio.h>


struct tampon{
    int *T; //tableau a traiter
    int deb; //indice de la case de depart
    int min, max;
    int minw,maxw;
};

void* calcul(void* arg){
    struct tampon * ptr = (struct tampon *)arg;
    
    for(int indice=ptr->deb;indice<100;indice += 2){
        if(ptr->min > ptr->T[indice])
        {
            ptr->min = ptr->T[indice];
            ptr->minw = indice;
        } 
        else 
            if(ptr->max < ptr->T[indice])
                {
                    ptr->max = ptr->T[indice];
                    ptr->maxw = indice;
                }
    }
}

void* calcul2(void* arg)
{
    struct tampon * ptr = (struct tampon *)arg;

    for(int indice=ptr->deb;indice<=ptr->deb+49;indice++)
    {
        if(ptr->min > ptr->T[indice])
        {
            ptr->min = ptr->T[indice];
            ptr->minw = indice;
        } 
        else 
            if(ptr->max < ptr->T[indice])
            {
                ptr->max = ptr->T[indice];
                ptr->maxw = indice;
            }
    }
}

int main(int argc, char* argv[])
{
    if (argc>2)
    {
        fprintf(stderr,"%s: un argument requis\n",argv[0]);
        exit(1);
    }
    int v = argc;
    int tab[100];
    time_t t;
    srand((unsigned) time(&t));
    pthread_t T1, T2;
    struct tampon st1, st2;
    int min,max,minw,maxw;
    for(int iteration = 0; iteration<100;iteration++)
    {
        tab[iteration] = rand()%501;
    }
    tab[99] = 999;
    tab[40] = -100;
    if(v==1)
    {
        printf("VERSION 0 \n");
        st1.T=tab; st1.deb=0; st1.min=st1.T[0]; st2.max=st1.T[0];
        st2.T=tab; st2.deb=1; st1.min=st1.T[0]; st2.max=st1.T[0];

        pthread_create(&T1, NULL, calcul, (void*) &st1);
        pthread_create(&T2, NULL, calcul, (void*) &st2);
    }
    else
    {
        printf("VERSION 1 \n");
        st1.T=tab; st1.deb=0; st1.min=st1.T[0]; st2.max=st1.T[0];
        st2.T=tab; st2.deb=50; st1.min=st1.T[0]; st2.max=st1.T[0];

        pthread_create(&T1, NULL, calcul2, (void*) &st1);
        pthread_create(&T2, NULL, calcul2, (void*) &st2);
    }
    
    
    pthread_join(T1,(void**) NULL);
    pthread_join(T2,(void**) NULL);
    
    if (st1.min < st2.min)
        {
            min =st1.min;
            minw = st1.minw;
        }
    else 
        {
            min = st2.min;
            minw = st2.minw;
        }

    if (st1.max > st2.max)
        {
            max =st1.max;
            maxw= st1.maxw;
        }
    else 
       {
            max = st2.max;
            maxw= st2.maxw;
       }

    printf("minimal : %d : %d \nmaximal : %d : %d \n",min,minw, max,maxw);
}