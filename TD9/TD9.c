#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <wait.h>
#include <semaphore.h>
#include <sys/mman.h>

//code pour les fonction
void AC1(); 
void AC2();
void AC3(); 
void AC4(); 
void AC5(); 

int main(){
    sem_t *S, *U, *M; //création des 3 sémaphore
    S = (sem_t*)mmap(NULL, sizeof(sem_t), PROT_READ|PROT_WRITE, MAP_SHARED|MAP_ANONYMOUS,0,0); 
    U = (sem_t*)mmap(NULL, sizeof(sem_t), PROT_READ|PROT_WRITE, MAP_SHARED|MAP_ANONYMOUS,0,0);
    M = (sem_t*)mmap(NULL, sizeof(sem_t), PROT_READ|PROT_WRITE, MAP_SHARED|MAP_ANONYMOUS,0,0);
    sem_init(S, 1, 0);
    sem_init(U, 1, 0);
    sem_init(M, 1, 2);

    int pid;
    pid = fork(); //fork ==> duplique utilisation programme pére idd, fils 0
    if(pid==-1){
        perror("TD9:fork");
        exit(1);
    }
    if(pid==0){
        sem_wait(M); //on prend une marmitte
        AC1(); //Sous programme créé par fork terminer dans ce script
        sem_post(S);//on pose une  bananette
        AC1();
        sem_post(S);//on pose une seconde bananette
        AC1();
        sem_post(S);//on pose la troisiéme
        sem_post(M); //on repose la marmitte
        exit(0); //fin du fils 1
    }

    pid = fork(); //fork ==> duplique utilisation programme pére idd, fils 0
    if(pid==-1){
        perror("TD9:fork");
        exit(1);
    }
    if(pid==0){
        sem_wait(S); //on attend une bananette
        sem_wait(M);//on attend une marmitte
        AC2(); //Sous programme créé par fork terminer dans ce script
        sem_post(U); //on pose la fraisade
        sem_post(M); //on repose la marmitte
        
        exit(0); //fin du fils 2
    }

    pid = fork(); //fork ==> duplique utilisation programme pére idd, fils 0
    if(pid==-1){
        perror("TD9:fork");
        exit(1);
    }
    if(pid==0){
        sem_wait(S); //on attend une bananette
        sem_wait(M);//on attend une marmitte
        AC3(); //Sous programme créé par fork terminer dans ce script
        sem_post(M);//on repose la marmitte
        sem_post(U);//on pose une anchoise
        exit(0);//fin du fils 3
    }

    pid = fork(); //fork ==> duplique utilisation programme pére idd, fils 0
    if(pid==-1){
        perror("TD9:fork");
        exit(1);
    }
    if(pid==0){
        sem_wait(S); //on attend une bananette
        sem_wait(M);//on attend une marmitte
        AC4(); //Sous programme créé par fork terminer dans ce script
        sem_post(M);//on repose la marmitte
        sem_post(U);//on pose une toulousaille
        exit(0); //fin du fils 4
    }

    pid = fork(); //fork ==> duplique utilisation programme pére idd, fils 0
    if(pid==-1){
        perror("TD9:fork");
        exit(1);
    }
    if(pid==0){
        sem_wait(U); //attente des trois plat
        sem_wait(U);
        sem_wait(U);
        sem_wait(M);//on attend une marmitte
        AC5(); //Sous programme créé par fork terminer dans ce script
        sem_post(M); //on repose une marmitte 
        exit(0);//fin du fils 5
    }


    wait(NULL); //attente pour la fermeture de fork1
    wait(NULL); //attente pour la fermeture de fork2
    wait(NULL); //attente pour la fermeture de fork3
    wait(NULL); //attente pour la fermeture de fork4
    wait(NULL); //attente pour la fermeture de fork5
    
    //destruction des 3 semaphore
    sem_destroy(U);
    sem_destroy(S);
    sem_destroy(M);

    exit(0);
}