#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <wait.h>

int main()
{
	pid_t pid;
	int i=0;
	int pipe(int descripteur[2]);

	printf("C'est parti !\n");
	pid=fork();
	
	wait(NULL);
	printf("%d\n",pid);
	i++;
	if(pid==0)
	{
	 i--;
	 printf("Im the child \n");
	}
	
	else
	 i++;

	printf("%d\n",i);
 	exit(2);
}
