#include<stdio.h> //définition des entrée sortie
#include<stdlib.h> //definition d'exit()
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h> //definis les fonction d'ouverture (open())
int argument(int argc)
{
    if (argc !=2)
    {
        /*
        * Test si 1 seul argument a été passer 
        */
        printf("1 seul argument attendu\n");
        return 1;
    }
    else
    {
        /*
        *Affichage du nom du fichier a ouvrir
        */
        return 0;
    }
}
int ouverture(char *test) //fonction qui test si on posséde les droit de lecture sur un fichier et qui retourne une description du fichier
{
    int d; 
    if((d=open(test,O_RDONLY))==-1)//test et ouverture d'un fichier "test" avec la méthode Read Only (0_RDNLY) si d =-1 alors une erreur sur le read a été trouver
    {
        printf("fichier impossible a ouvrir\n");
        return 1; //en cas d'erreur on retourne 1
    }
    else
    {
        printf("fichier ouvert\n");
        return d;//si tout ce passe bien on retourne d qui est le descripteur du fichier
    }

    
}
void traitement(float tab[3],float recu) 
/*
 *Fonction traitement
 * recois un tableau de trois élément float et ainsi qu'un float recu représentant la valeur a tester
 * et ne retourne rien
 */
{
    if(recu>tab[1])//si la valeur recu est supérieur aux maximum du tableau alors elle devient le maximum
    {
        tab[1]=recu;
    }
    tab[0]=tab[0]+recu;//on ajoute la valeur a la moyenne qui sera diviser plus tard par 50
    if(recu<tab[2])//si la valeur est inférieur aux minimum du tableau alors tab[2] qui représente le minimum recois recu
    {
        tab[2]=recu;
    }
}
void lecture(int ouvert,float tab[3])
/*
 *Fonction lecture qui va lire 50 valeur et qui va appeller traitement qui retournera dans un tableau fournie triera les valeur en sortant la moyenne,le min et le max
 */
{
int iterationx;
    float swap;
    for (iterationx=0;iterationx<50;iterationx++) //iteration de 0 a 50
    {
        read(ouvert,&swap,sizeof(float)); //lecture d'une ligne du fichier
        traitement(tab,swap); //appelle d'une fonction traitement qui testera le min/max et ajoutera a la moyenne
    }
    tab[0]=(tab[0])/50.0;//sachant que chaque espéce posséde 50 valeur on divise les valeur recu par 50 
}
void lectureETtraitement(int ouvert,float premier[3],float second[3],float troisieme[3]) 
/*
 *Fonction de lecture et traitement des valeur dans le fichier decrit dans ouvert
 * Les argument premeier,second et troisieme sont trois tableau de float contenant 3 valeur, la premiére représente la moyenne,la seconde le max et la quatriéme le min
 * Cette fonction n'a pour uttilisation que d'appelle lecture
 */
{
    lecture(ouvert,premier);
    lecture(ouvert,second);
    lecture(ouvert,troisieme);
}

void Affichage(float tab[3])
//fonction d'affichage d'un tableau de float ayant pour premiére valeur sa moyenne,la seconde son max et la derniére son minimum
{
    printf("Espece Iris Setosa:\n\t Moyenne: %f \n\t Max: %f \n\t Min: %f\n",tab[0],tab[1],tab[2]);
}

void fermeture(int descripteur)
{
    close(descripteur); //fermeture de la connection aux fichier dans descripteur
}


int main(int argc, char *argv[])
{
    int open;
    float premier[]={0,0,1000000},second[]={0,0,1000000},troisieme[]={0,0,10000};//le premier élément des tableau est la moyenne, le second le max et le troisiéme le minimum
    if(argument(argc)==1) //appelle de fonction argument qui sert a tester les argument via argc qui correspond a la quantité
    {
        exit(1); //sortie du programme avec erreur 1 qui correspond a une erreur d'argument
    }
    open=ouverture(argv[1]); //appelle de la fonction open qui ouvrira le fichier et nous retournera 1 en cas d'erreur de droit sur le fichier
    if(open==1){exit(2);} //sortie du programme avec erreur 2 qui correspond a une erreur d'ouverture de fichier
    
    lectureETtraitement(open,premier,second,troisieme);//appelle de la fonction lectureETtraitement qui lira les 150 entrer et les traitera en les appliquant dans trois tableau transmis
    Affichage(premier); Affichage(second); Affichage(troisieme); //appelle de la fonction d'affichage des tableau
    fermeture(open);
    exit(0);
}
