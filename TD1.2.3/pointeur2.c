#include <stdio.h>
#include <stdlib.h>

int main() 
{
	double valeur = 65.21;
	double *pv;

	int val = 7, *ptr;		// ptr est un pointeur sur entier

	pv = &valeur;
	ptr = &val;

	printf("taille valeur : %ld octets\n", sizeof(valeur));
	printf("taille pv : %ld octets\n", sizeof(pv));

	printf("taille val : %ld octets\n", sizeof(val));
	printf("taille ptr : %ld octets\n", sizeof(ptr));
	
	exit(0);
} 