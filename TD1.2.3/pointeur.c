#include <stdio.h>
#include <stdlib.h>


int main()
{
	double valeur;  //déclaration d'un réel double précision
    double *pv;     //déclaration d'un pointeur sur réel double préc.

    valeur = 37.2;
    pv = &valeur;

    printf("valeur = %f\n",valeur);
    printf("*pv = %f\n", *pv);
    printf("&valeur = %p\n",&valeur);
    printf("pv = %p\n", pv);
    printf("*pv+1 = %lf\n",*pv+1);
    exit(0);
}
