#include <stdio.h>
#include <stdlib.h>

/*
Fonction multi
fonction de multiplication d'un tableaux par un autre retournant la somme de chaque multiplication
*/
int multi(int *t1,int *t2,int tail1,int tail2)
{
    int retour=0;

    for (int iteration =0;iteration<tail1;iteration++) //boucle t2
    {
        for(int iteration2 = 0; iteration2<tail2;iteration2++) //boucle T1
        {
            retour= t2[iteration2] * t1[iteration] + retour; //multi d'une casse de t1 par celle de t2 + retour
        }
    }
    return retour;
}

int main()
{
    int t1[] = {4,8,7,12};
    int t2[] = {3,6};
	printf("le schtroumph est de %d\n", multi(t1,t2,4,2));//transfert des tableaux + des taille
	exit(0);
}
