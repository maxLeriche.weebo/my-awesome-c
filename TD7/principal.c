#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <pwd.h>
#include <grp.h>
#include <dirent.h>

int main(int argc, char* argv[])
{
    // si trop d'agument sont présent
    if (argc>2)
    {
        fprintf(stderr,"%s: un argument requis\n",argv[0]);
        exit(1);
    }

    struct dirent *st;
    struct stat fic;
    DIR *rep;
    if((rep = opendir(argv[1])) == NULL) //test si on ne peux écrire ou on ne posséde pas les droit
    {
        fprintf(stderr, "%s: repertoire non ouvrable ou inexistant;", argv[1]); perror("");
        exit(2);
    }
    else //si tout est OK alors on continue
    {   
        chdir(argv[1]); //changement du dossier de travaille pour la suite des commande
        while((st = readdir(rep)) != NULL) //itération a travers tout les élément dans le dossier et application uttilisation de la strucutre st
        {
            stat(st->d_name, &fic);
            printf("Fichier étudié \t\t : %s\n", st->d_name); //affiche le nom du fichier
            printf("Numéro de i-noeud \t : %d\n", st->d_ino); //affiche le numéro de i-noeud
            printf("Nombre de liens \t : %d\n", fic.st_nlink); //affiche le nombre de lien >1 si le fichier posséde d'autre lien
            printf("Propriétaire \t\t : %d (%s)\n", fic.st_uid, getpwuid(fic.st_uid)->pw_name);//affichage et traduction du propriétaire ainsi que du nom du propriétaire
            printf("Groupe propriétaire \t : %d (%s)\n", fic.st_gid, getgrgid(fic.st_gid)->gr_name); //affichage et traduction du groupe propriétaire et du nom du groupe
            printf("Taille en octets \t : %d\n", fic.st_size); //affichage de la taille du fichier en byte 1byte = 1 octet
            if(S_ISDIR(fic.st_mode)) //teste de quelle type est le fichier ici on test si c'est un fichier
                printf("Type \t : Répertoire\n");
            else
                if(S_ISREG(fic.st_mode)) //teste si le fichier est regulier
                    printf("Type \t : Régulier \n");
                else //teste si le fichier est d'un autre type
                    printf("Type \t : Autre \n");
            printf("Date de dernière modification du i-noeud: %s", ctime(&fic.st_ctime));
            printf("Date de dernière modification du fichier: %s", ctime(&fic.st_mtime));
            printf("Date de dernier accès au fichier: %s", ctime(&fic.st_atime));
            printf("----------------------------------------------\n");
            
        }
        closedir(rep);
        exit(0);
    }
    
}
