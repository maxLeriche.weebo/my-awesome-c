#include <unistd.h>
#include <stdio.h>
#include <math.h>
#include <wait.h>
#include <stdlib.h>
#include <time.h>

#define size 8


void flecture(int descripteur[2],int iteration) //fonction du Fils 2
{
    close(descripteur[1]); //on ferme le coter ecriture car le fils 2 nécrit pas
    int m=0;
    int n=0;
    for(int boucle=0;boucle<iteration;boucle++)//boucle qui sert a lire iteration couple de x,y
    {
        float value[2];
        read(descripteur[0],value,sizeof(float[2])); //remplissage du tableau value a partir des donnée dans le tab
        printf("je lis %d de x: %f et y: %f\n",boucle,value[0],value[1]); //Sert a savoir ce que l'on recois
        if((value[0]>=0&&value[0]<=1)&&(value[1]>=0&&value[1]<=1)) //test pour l'incrémentation de n
            {
                n++;
            }
            if((value[0]*value[0])+(value[1]*value[1])<=1) //test pour l'incrémentation de m
            {
                m++;
            }
        }
        close(descripteur[0]); // on ferme le desripteur pour dire qu'on a finis la lecture
        printf("M=%d  N=%d\n",m,n);  //display de m et n 
        printf("PI ~= %f\n",(4.0*(float)m)/(float)n); //calcule de pi
        return;
}

void fecriture(int descripteur[2], int iteration) //fonction du fils 1
{
    float a = 1.0;
    for(int boucle=0;boucle<iteration;boucle++) //boucle qui sert a générer iteration couple de x,y
    {
        float value[2];
        value[0]=((float)rand()/(float)(RAND_MAX) * a);
        value[1] =((float)rand()/(float)(RAND_MAX) * a);
        printf("j'écris %d de x: %f et y: %f\n",boucle,value[0],value[1]); //sert a savoir ce que tu écrit (a comparer avec la lecture)
        write(descripteur[1],value,sizeof(float[2]));
    }
    close(descripteur[1]); //on ferme le descripteur pour dire qu'on a finis l'écriture
    return;
}

int main()
{
    srand((unsigned int)time(NULL)); //création du random
    //Création des deux fils
    pid_t F1;
    pid_t F2;
    //création de la variable d'itération(nombre de Y)
    int iteration =0;
    //création du pipe
    int descripteur[2];
    if(pipe(descripteur)!=0) //gestion de l'erreur de pipee
    {
        perror("Erreur dans l'instanciation du descripteur");
        exit(2);
    }

    printf("Debut du calcul\n");
    printf("combien de couple(x,y)? : ");
    int res =scanf("%d", &iteration); //gestion d'erreur du scanf
    if (res != 1)
    {
        perror("Vous avez fait une erreur lors de la saisie.");
        exit(1);
    }
    else if(iteration < 1)
    {
        perror("Erreur valeur trop faible");
        exit(1);
    }
    if((F1=fork())==-1) //on lance le processus fils F1
    {
        perror("Erreur dans l'instanciation du Fils 1");
        exit(3);
    }
    if (F1!=0) //ici on interdit aux processus fils F1 de lancer un procesus fils F2
    {
        if((F2=fork())==-1) //on lance le processus fils F2
        {
            perror("Erreur dans l'instanciation du Fils 2");
            exit(3);
        }
    }
    if (F1==0) //ici on est retrouve les instruction de F1
    {
        fecriture(descripteur,iteration);
        exit(0);
    }
    else if(F2==0)//ici on est retrouve les instruction de F2
    {
        flecture(descripteur,iteration);
        exit(0);
    }
    
    //printf("j'attend de survivre a mes gosse\n");
    
    wait(NULL);
   
    wait(NULL);
    
    exit(0);
}
