#include "../temps.h"
//fonction de somme d'heure/minute partant de quatre entier qui retourne un objet Time
Temps sommeInt(int heure,int minute,int heure2,int minute2)
{
    int swapheure=heure+heure2;
    int swapminute = (minute+minute2);
    int divide=swapminute/60;
    Temps *retour;
    retour=(struct temps*)malloc(sizeof(struct temps));
    retour->h=swapheure+divide;
    retour->min=swapminute%60;
    
    return *retour;
}
//fonction de somme heure/miinute partant de deux objet temps qui retourne un objet temps
Temps sommeType(Temps swap1,Temps swap2)
{
    int swapheure=swap1.h+swap2.h;
    int swapminute = (swap1.min+swap2.min);
    int divide=swapminute/60;
    Temps *retour;
    retour=(struct temps*)malloc(sizeof(struct temps));
    retour->h=swapheure+divide;
    retour->min=swapminute%60;
    return *retour;
}