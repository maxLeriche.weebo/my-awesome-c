#include "../temps.h"

//fonction de conversion de deux entier en un seule (heure*60)+ minute
int conversionInt(int heure,int minute)
{
    return (heure*60)+minute;
}
//fonction de conversion d'un objet temps en un int
int conversionType(Temps time)
{
    return (time.h*60)+time.min;
}